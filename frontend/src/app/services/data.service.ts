import { Injectable } from '@angular/core';
import { Feathers } from './feathers.service';
// import Car from '../car';
/**
 *  Abstraction layer for data management.
 */
@Injectable()
export class DataService {
  constructor(private feathers: Feathers) {
  }

  things$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('things'))
      .watch()
      .find();
  }
  
  cars$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('cars'))
      .watch()
      .find();
  }
  
  addThing(desc: string) {
    if (desc === '') {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('things')
      .create({
        desc
      });
  }

  addCar(year:number, make:string, mileage:number, model:string) {
    
    if ( year < 1850 || year > 2020 ) {
      return;
    }
    if ( make === '' ) {
      return
    }
    
    if (mileage < 0 ) {
      return
    }
    
    if (  model === '' ) {
      return
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('cars')
      .create({
       make,
       model,
       year,
       mileage
      });
  }
  
  deleteCar(carData : any){
    this.feathers
    .service('cars')
    .remove(
      carData.id
    )
  }
}
