import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Paginated } from '@feathersjs/feathers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from '../services/data.service';

@Component({
  
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
  
})


export class CarsComponent implements OnInit {

  cars$:Observable<any[]>;
  carForm;

  constructor( 
    private data:DataService,
    private formBuilder: FormBuilder
    ) {
      this.cars$ =  data.cars$().pipe(
        map((c:Paginated<any>) => c.data)
      );
  }

  createCar(carInfo) {
    this.data.addCar( carInfo.year, carInfo.make, carInfo.model, carInfo.mileage );
  }

  ngOnInit(): void {
    this.carForm = this.formBuilder.group({
      make:['', [Validators.required] ],
      model: ['', [Validators.required] ],
      year: ['', [Validators.required] ],
      mileage: ['', [Validators.required] ]
    })
  }
  
  deleteCar(carInfo){
    this.data.deleteCar(carInfo);
  }

}
